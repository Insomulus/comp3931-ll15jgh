

p1 = Category(name='Chest')
p1.save()
p2 = Category(name='Back')
p2.save()
p3 = Category(name='Arms')
p3.save()
p4 = Category(name='Abdominals')
p4.save()
p5 = Category(name='Legs')
p5.save()
p6 = Category(name='Shoulders')
p6.save()


e1 = Exercise(name="Barbell Bench Press", description="A barbell bench press using a bar on a bench.",
              instructions="Push the bar from the chest on a bench.", type="freeweight", image="exercise_images/barbell_bench_press.png")
e1.save()
e1.category.add(p1)

e2 = Exercise(name="Chest Fly Machine", description="A Chest Fly Machine",
              instructions="A Chest Fly Machine", type="machine", image="exercise_images/chest_fly_machine.png")
e2.save()
e2.category.add(p1)
e3 = Exercise(name="Banded Chest Fly", description="A Banded Chest Fly",
              instructions="A Banded Chest Fly", type="bands", image="exercise_images/banded_chest_fly.png")

e3.save()
e3.category.add(p1)
e4 = Exercise(name="Push-Ups", description="A push-up",
              instructions="A push-up", type="calisthenics", image="exercise_images/push_up.png")
e4.save()
e4.category.add(p1)


e2 = Exercise(name="Barbell Deadlift", description="a deadlift",
              instructions="A deadlift", type="freeweight", image="exercise_images/barbell_deadlift.png")
e2.save()
e2.category.add(p2)
e3 = Exercise(name="Lat Pull-Down", description="A lat pull down",
              instructions="A lat pulld down", type="machine", image="exercise_images/lat_pull_down.png")

e3.save()
e3.category.add(p2)
e4 = Exercise(name="Pull-Ups", description="A pull-up",
              instructions="A pull-up", type="calisthenics", image="exercise_images/pull_up.png")

e4.save()
e4.category.add(p2)
e3 = Exercise(name="Banded Pull-Ups", description="A banded pull-up",
              instructions="A banded pull-up", type="bands", image="exercise_images/banded_pull_up.png")
e3.save()
e3.category.add(p2)


e2 = Exercise(name="Bicep Curls", description="a bicep curl",
              instructions="A bicep curl", type="freeweight", image="exercise_images/bicep_curls.png")
e2.save()
e2.category.add(p3)
e3 = Exercise(name="Tricep Push-Downs", description="A tricep push down",
              instructions="A tricep push down", type="machine", image="exercise_images/tricep_push_down.png")
e3.save()
e3.category.add(p3)
e3 = Exercise(name="Chin-Ups", description="A chin-up",
              instructions="A chin-up", type="calisthenics", image="exercise_images/chin_up.png")

e3.save()
e3.category.add(p3)
e3 = Exercise(name="Banded Bicep Curls", description="A banded bicep curl",
              instructions="A banded bicep curl", type="bands", image="exercise_images/banded_bicep_curls.png")
e3.save()
e3.category.add(p2)


e2 = Exercise(name="Plated Ab Crunch", description="a plated ab crunch",
              instructions="A plated ab crunch", type="freeweight", image="exercise_images/plate_ab_crunch.png")
e2.save()
e2.category.add(p4)
e3 = Exercise(name="Ab Crunch Machine", description="A machine ab crunch",
              instructions="A machine ab crunch", type="machine", image="exercise_images/machine_ab_crunch.png")
e3.save()
e3.category.add(p4)
e3 = Exercise(name="Leg Raises", description="A leg raise",
              instructions="A leg raise", type="calisthenics", image="exercise_images/leg_raises.png")
e3.save()
e2.category.add(p4)
e3 = Exercise(name="Banded Bridge", description="A banded bridge",
              instructions="A banded bridge", type="bands", image="exercise_images/banded_bridge.png")
e3.save()
e3.category.add(p4)


e2 = Exercise(name="Barbell Squat", description="a squat",
              instructions="A squat", type="freeweight", image="exercise_images/barbell_squat.png")
e2.save()
e2.category.add(p5)
e3 = Exercise(name="Leg Press", description="A machine leg press",
              instructions="A machine leg press", type="machine", image="exercise_images/leg_press.png")
e3.save()
e3.category.add(p5)
e3 = Exercise(name="Pistol Squats", description="A pistol squat",
              instructions="A pistol squat", type="calisthenics", image="exercise_images/pistol_squat.png")
e3.save()
e3.category.add(p5)
e3 = Exercise(name="Banded Leg Extension", description="A banded leg extension",
              instructions="A banded leg extension", type="bands", image="exercise_images/banded_leg_extension.png")
e3.save()
e3.category.add(p5)

e2 = Exercise(name="Barbell Shrugs", description="a barbell shrug",
              instructions="A barbell shrug", type="freeweight", image="exercise_images/barbell_shrugs.png")
e2.save()
e2.category.add(p6)
e3 = Exercise(name="Rear Delt Reverse Fly", description="A rear delt reverse fly",
              instructions="A rear delt reverse fly", type="machine", image="exercise_images/rear_delt_reverse_fly.png")

e3.save()
e3.category.add(p5)
e3 = Exercise(name="Pike Push-Ups", description="A pike push up",
              instructions="A pike push up", type="calisthenics", image="exercise_images/pike_push_up.png")

e3.save()
e3.category.add(p5)
e3 = Exercise(name="Banded Lateral Raises", description="A banded lateral raise",
              instructions="A banded lateral raise", type="bands", image="exercise_images/banded_lateral_raise.png")
e3.save()
e3.category.add(p5)
