"""ophf URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.urls import path
from overpowered.views import register, log_in, log_out, pull_sets, edit_set, pull_exercises, add_exercise_instance, create_set, delete_set, delete_exercise_instance, create_exercise, pull_set_graph_data, pull_progress, pull_user_details, get_location

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/register/', register),
    path('api/login/', log_in),
    path('api/log_out/', log_out),
    path('api/pull_sets/', pull_sets),
    path('api/edit_set/', edit_set),
    path('api/pull_exercises/', pull_exercises),
    path('api/add_exercise_instance/', add_exercise_instance),
    path('api/create_set/', create_set),
    path('api/delete_set/', delete_set),
    path('api/delete_exercise_instance/', delete_exercise_instance),
    path('api/create_exercise/', create_exercise),
    path('api/pull_set_graph_data/', pull_set_graph_data),
    path('api/pull_progress/', pull_progress),
    path('api/pull_user_details/', pull_user_details),
    path('api/get_location/', get_location)
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
