from django.http import HttpResponse
from .models import UserAccount, Set, Session, Exercise, ExerciseInstance, Category
from django.contrib.auth import authenticate, logout, login
from django.views.decorators.csrf import csrf_exempt
import json
import datetime
import base64
from geopy.geocoders import Nominatim
import geopy.geocoders
from django.conf import settings
import certifi
import ssl

ctx = ssl.create_default_context(cafile=certifi.where())
geopy.geocoders.options.default_ssl_context = ctx
# Create your views here.


@csrf_exempt
def register(request):
    print("test")
    if request.method != 'POST':
        return HttpResponse(content='Only POST requests allowed.',
                            content_type='text/plain', status=400)
    else:
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        height = request.POST['height']
        weight = request.POST['weight']
        date_of_birth = request.POST['date_of_birth']
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        print(password)
        if not UserAccount.objects.filter(username=username).exists():
            if not UserAccount.objects.filter(email=email).exists():
                user = UserAccount(
                    first_name=first_name, last_name=last_name, height=height,
                    weight=weight, date_of_birth=date_of_birth,
                    username=username, email=email)
                print(user)
                user.save()
                user = UserAccount.objects.get(username=username)
                user.set_password(password)
                print(user)

                print(user.password)
                print(user.username)
                user.save()
                return HttpResponse(content=username+' has been registered sucessfully, welcome: '+first_name, content_type='text/plain', status=200, reason='OK')
            else:
                return HttpResponse(content=email+' is already registered to another user, please try another email', content_type='text/plain', status=402, reason='Email')
        else:
            return HttpResponse(content=username+' is already registered to another user, please try another username', content_type='text/plain', status=401, reason='Username')


@csrf_exempt
def log_in(request):
    if request.method != 'POST':
        return HttpResponse(content='Only POST requests allowed.', content_type='text/plain', status=400)
    else:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        print(username)
        print(password)
        print(UserAccount.objects.filter(
            username=username, password=password).query)
        print(user)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponse(content=username+' has sucessfully logged in, welcome!', content_type='text/plain', status=200, reason='OK')
            else:
                return HttpResponse(content='The user "'+username+'" is disabled.', content_type='text/plain', status=401, reason='Disabled')
        else:
            return HttpResponse(content='Invalid usernname or password, please try again.', content_type='text/plain', status=402, reason='Invalid username/password')


@csrf_exempt
def check(request):
    result = auth(request)
    if result == True:
        return HttpResponse(content='authenticated', content_type='text/plain', status=200, reason='OK')
    else:
        return HttpResponse(content='not authenticated', content_type='text/plain', status=401, reason='Not logged in')


@csrf_exempt
def auth(creds):
    user = authenticate(
        username=creds.POST['username'], password=creds.POST['password'])
    print(user)
    if user is not None:
        print("not none")
        if user.is_active:
            print("active")
            return True
        else:
            print("other")
            return False
    else:
        print("other")
        return False


@csrf_exempt
def log_out(request):
    if request.method != 'POST':
        return HttpResponse(content='Only POST requests allowed.', content_type='text/plain', status=400)
    else:
        print(request)
        if check(request):
            logout(request)
            return HttpResponse(content='You have sucessfully logged out, goodbye!\n', content_type='text/plain', status=200, reason='OK')
        else:
            return HttpResponse(content='You are not logged in.\n', content_type='text/plain', status=401, reason='Not logged in')


@csrf_exempt
def pull_sets(request):
    if request.method != 'POST':
        return HttpResponse('Only POST requests allowed.', content_type='text/plain', status=400)
    else:
        username = request.POST['username']
        session = request.POST['date']
        print(session)
        date = datetime.datetime.strptime(
            session, '%d-%m-%Y').strftime('%Y-%m-%d')
        print(date)
        if Session.objects.filter(user_id=username, date=date).exists():
            print('query is not none')
            sessions = Session.objects.filter(
                user_id=username, date=date).values().last().get('id')
            instances = ExerciseInstance.objects.filter(session_id=sessions)

            print('Printing all instances ', instances, '\n')
            # diary = Diary.objects.filter(user=username).values().last().get('id')
            set_list = []
            instance_list = []
            #exercise_list = []

            for instance in instances:
                print('Printing single instance: ', instance, '\n')
                parsed_instances = {'time': str(
                    instance.time), 'instance_id': instance.id, 'exercise': instance.exercise_id}
                instance_list.append(parsed_instances)
                sets = Set.objects.filter(instance_id=instance.id)
                print('Printing all sets for this instance: ', sets, '\n')
                for set in sets:
                    parsed_sets = {'time': str(set.time), 'weight': set.weight, 'resistance': set.resistance,
                                   'bodyweight': set.bodyweight, 'added_weight': set.added_weight,
                                   'reps': set.reps, 'instance_id': set.instance_id, 'set_id': set.id}
                    set_list.append(parsed_sets)
                    print('Printing instances list', instance_list, '\n')
                    print('Printing set list: ', set_list, '\n')
            print('Printing final instances list', instance_list, '\n')
            print('Printing final set list: ', set_list, '\n')
            payload = {'exercise_instances': instance_list,
                       'sets': set_list}
            return HttpResponse(content=json.dumps(payload), content_type='text_plain', status=200, reason='ok')
        else:
            print('no date')
            return HttpResponse(content='User does not have a session for this date',
                                content_type='text/plain', status=404, reason='none')


@csrf_exempt
def edit_set(request):
    if request.method != 'POST':
        return HttpResponse('Only POST requests allowed.', content_type='text/plain', status=400)
    else:
        username = request.POST['username']
        set_id = int(request.POST['set_id'])
        weight = int(request.POST['weight'])
        resistance = int(request.POST['resistance'])
        body_weight = int(request.POST['body_weight'])
        added_weight = int(request.POST['added_weight'])
        reps = int(request.POST['reps'])
        print(username, set_id, weight, resistance,
              body_weight, added_weight, reps)
        query = Set.objects.filter(id=set_id)
        if query.exists():
            print(query)
            query.update(weight=weight, resistance=resistance, bodyweight=body_weight,
                         added_weight=added_weight, reps=reps)
            return HttpResponse(content='Edit successfully applied.', content_type='text/plain', status=200, reason='ok')
        else:
            return HttpResponse(content='This set does not exist', content_type='text/plain', status=404, reason='none')


def convert_image(image_path):
    image_base64 = base64.b64encode(image_path.read()).decode('utf-8')
    return image_base64


def decode_image(base_64_image, name):
    with open(settings.MEDIA_ROOT+name + ".png", "wb") as fh:
        fh.write(base64.b64decode(base_64_image))
    return name + '.png'


def pull_exercises(request):
    if request.method != 'GET':
        return HttpResponse('Only GET requests allowed.', content_type='text/plain', status=400)
    else:
        exercises = Exercise.objects.all()
        categories = Category.objects.all()
        exercise_list = []
        category_list = []

        for category in categories:
            category_list.append(category.name)

        for exercise in exercises:
            exercise_category_list = []
            image_base64 = convert_image(exercise.image)
            categories = exercise.category.all()
            for category in categories:
                parsed_category = {'category': category.name}
                exercise_category_list.append(parsed_category)
            parsed_exercises = {'name': exercise.name, 'description': exercise.description, 'instructions': exercise.instructions,
                                'image': image_base64, 'type': exercise.type, 'category': exercise_category_list}
            exercise_list.append(parsed_exercises)
        print(exercise_list)
        print(category_list)
        payload = {'exercises': exercise_list,
                   'categories': category_list}
        return HttpResponse(content=json.dumps(payload), content_type='text_plain', status=200, reason='ok')


@csrf_exempt
def add_exercise_instance(request):
    if request.method != 'POST':
        return HttpResponse('Only POST requests allowed.', content_type='text/plain', status=400)
    else:
        username = request.POST['username']
        date = request.POST['date']
        print(datetime.datetime.strptime(date, "%d-%m-%Y").strftime("%Y-%m-%d"))
        formatted_date = datetime.datetime.strptime(
            date, "%d-%m-%Y").strftime("%Y-%m-%d")
        exercise_name = request.POST['exercise']
        session = Session.objects.filter(date=formatted_date, user_id=username)
        if not session.exists():
            new_session = Session(date=formatted_date, user_id=username)
            new_session.save()
        session = Session.objects.get(date=formatted_date, user_id=username)
        exercise_instance = ExerciseInstance(
            time=datetime.datetime.now(), session_id=session.id, exercise_id=exercise_name, user_id=username)
        exercise_instance.save()
        return HttpResponse(content='Exercise instance succesfully created.', content_type='text/plain', status=200, reason='ok')


@csrf_exempt
def create_set(request):
    if request.method != 'POST':
        return HttpResponse('Only POST requests allowed.', content_type='text/plain', status=400)
    else:
        username = request.POST['username']
        weight = request.POST['weight']
        resistance = request.POST['resistance']
        body_weight = request.POST['body_weight']
        added_weight = request.POST['added_weight']
        reps = request.POST['reps']
        instance = request.POST['instance']
        exercise = request.POST['exercise']
        set = Set(time=datetime.datetime.now(), instance_id=instance, weight=weight, resistance=resistance, bodyweight=body_weight,
                  added_weight=added_weight, reps=reps, user_id=username, exercise_id=exercise)
        set.save()
        return HttpResponse(content='Set succesfully created.', content_type='text/plain', status=200, reason='ok')


@csrf_exempt
def delete_set(request):
    if request.method != 'POST':
        return HttpResponse('Only POST requests allowed.', content_type='text/plain', status=400)
    else:
        set_id = request.POST['set_id']
        set = Set.objects.filter(id=set_id)
        if set.exists():
            set.delete()
        else:
            return HttpResponse(content='This set does not exist', content_type='text/plain', status=404, reason='none')
        return HttpResponse(content='Set succesfully deleted.', content_type='text/plain', status=200, reason='ok')


@csrf_exempt
def delete_exercise_instance(request):
    if request.method != 'POST':
        return HttpResponse('Only POST requests allowed.', content_type='text/plain', status=400)
    else:
        exercise_id = request.POST['instance_id']
        exercise_instance = ExerciseInstance.objects.get(id=exercise_id)
        current_session = exercise_instance.session_id
        if exercise_instance:
            exercise_instance.delete()
            if not ExerciseInstance.objects.filter(session_id=current_session).exists():
                Session.objects.filter(id=current_session).delete()

        else:
            return HttpResponse(content='This set of exercises does not exist', content_type='text/plain', status=404, reason='none')
        return HttpResponse(content='Set of exercises succesfully deleted.', content_type='text/plain', status=200, reason='ok')


@csrf_exempt
def create_exercise(request):
    if request.method != 'POST':
        return HttpResponse('Only POST requests allowed.', content_type='text/plain', status=400)
    else:
        name = request.POST['name']
        description = request.POST['description']
        instructions = request.POST['instructions']
        categories = request.POST['categories'].strip('][').split(', ')
        type = request.POST['type']
        image = decode_image(request.POST['image'], name)
        if Exercise.objects.filter(name=name).exists():
            return HttpResponse(content='This exercise already exists', content_type='text/plain', status=404, reason='none')
        else:
            exercise = Exercise(name=name, description=description,
                                instructions=instructions, image=image, type=type)
            exercise.save()
            print(categories)
            for category in categories:
                print(category)
                cat = Category.objects.get(name=category)
                exercise.category.add(cat)
        return HttpResponse(content='Set succesfully created.', content_type='text/plain', status=200, reason='ok')


@csrf_exempt
def pull_set_graph_data(request):
    if request.method != 'POST':
        return HttpResponse('Only POST requests allowed.', content_type='text/plain', status=400)
    else:
        username = request.POST['username']
        exercise = request.POST['exercise']
        sets = Set.objects.filter(user_id=username, exercise_id=exercise)
        set_list = []
        if not sets.exists():
            return HttpResponse(content='No sets for this exercise', content_type='text/plain', status=404, reason='none')
        for set in sets:
            parsed_sets = {'time': str(
                set.time), 'weight': set.weight, 'reps': set.reps}
            set_list.append(parsed_sets)

        payload = {'sets': set_list}
        return HttpResponse(content=json.dumps(payload), content_type='text_plain', status=200, reason='ok')


@csrf_exempt
def pull_progress(request):
    if request.method != 'POST':
        return HttpResponse('Only POST requests allowed.', content_type='text/plain', status=400)
    else:
        username = request.POST['username']
        sessions = Session.objects.filter(user_id=username)
        exercise_list = []
        if sessions.exists():
            for session in sessions:
                exercises = ExerciseInstance.objects.filterby(
                    session_id=session)
                for exercise in exercises:
                    exercise_list.append(exercise.exercise_id)
                payload = {'exercises': exercise_list}
            return HttpResponse(content=json.dumps(payload), content_type='text_plain', status=200, reason='ok')
        else:
            print('no date')
            return HttpResponse(content='No exercise data',
                                content_type='text/plain', status=404, reason='none')


@csrf_exempt
def pull_user_details(request):
    if request.method != 'POST':
        return HttpResponse('Only POST requests allowed.', content_type='text/plain', status=400)
    else:
        user_id = request.POST['username']
        user = UserAccount.objects.get(username=user_id)
        print(user_id)
        print(user)
        if user:
            details = {'username': user.username, 'email': user.email, 'first_name': user.first_name,
                       'last_name': user.last_name, 'height': user.height, 'weight': user.weight}
            payload = {'details': details}
            print(payload)
            return HttpResponse(content=json.dumps(payload), content_type='text_plain', status=200, reason='ok')
        else:
            print('no date')
            return HttpResponse(content='No user exists',
                                content_type='text/plain', status=404, reason='none')


@csrf_exempt
def get_location(request):
    if request.method != 'POST':
        return HttpResponse('Only POST requests allowed.', content_type='text/plain', status=400)
    else:
        latitude = float(request.POST['latitude'])
        longitude = float(request.POST['latitude'])
        print(latitude)
        print(longitude)
        try:
            geolocator = Nominatim(user_agent='overpowered', scheme='http')
            location = geolocator.reverse((latitude, longitude))
            road = location.raw['address']['road']
            return HttpResponse(content=json.dumps(road), content_type='text_plain', status=200, reason='ok')
        except:
            print("fail")
            return HttpResponse(content='Failed to get road', content_type='text/plain', status=404, reason='none')


def run():

    p1 = Category(name='Chest')
    p1.save()
    p2 = Category(name='Back')
    p2.save()
    p3 = Category(name='Arms')
    p3.save()
    p4 = Category(name='Abdominals')
    p4.save()
    p5 = Category(name='Legs')
    p5.save()
    p6 = Category(name='Shoulders')
    p6.save()

    e1 = Exercise(name="Barbell Bench Press", description="A barbell bench press using a bar on a bench.",
                  instructions="Push the bar from the chest on a bench.", type="freeweight", image="exercise_images/barbell_bench_press.png")
    e1.save()
    e1.category.add(p1)

    e2 = Exercise(name="Chest Fly Machine", description="A Chest Fly Machine",
                  instructions="A Chest Fly Machine", type="machine", image="exercise_images/chest_fly_machine.png")
    e2.save()
    e2.category.add(p1)
    e3 = Exercise(name="Banded Chest Fly", description="A Banded Chest Fly",
                  instructions="A Banded Chest Fly", type="bands", image="exercise_images/banded_chest_fly.png")

    e3.save()
    e3.category.add(p1)
    e4 = Exercise(name="Push-Ups", description="A push-up",
                  instructions="A push-up", type="calisthenics", image="exercise_images/push_up.png")
    e4.save()
    e4.category.add(p1)

    e2 = Exercise(name="Barbell Deadlift", description="a deadlift",
                  instructions="A deadlift", type="freeweight", image="exercise_images/barbell_deadlift.png")
    e2.save()
    e2.category.add(p2)
    e3 = Exercise(name="Lat Pull-Down", description="A lat pull down",
                  instructions="A lat pulld down", type="machine", image="exercise_images/lat_pull_down.png")

    e3.save()
    e3.category.add(p2)
    e4 = Exercise(name="Pull-Ups", description="A pull-up",
                  instructions="A pull-up", type="calisthenics", image="exercise_images/pull_up.png")

    e4.save()
    e4.category.add(p2)
    e3 = Exercise(name="Banded Pull-Ups", description="A banded pull-up",
                  instructions="A banded pull-up", type="bands", image="exercise_images/banded_pull_up.png")
    e3.save()
    e3.category.add(p2)

    e2 = Exercise(name="Bicep Curls", description="a bicep curl",
                  instructions="A bicep curl", type="freeweight", image="exercise_images/bicep_curls.png")
    e2.save()
    e2.category.add(p3)
    e3 = Exercise(name="Tricep Push-Downs", description="A tricep push down",
                  instructions="A tricep push down", type="machine", image="exercise_images/tricep_push_down.png")
    e3.save()
    e3.category.add(p3)
    e3 = Exercise(name="Chin-Ups", description="A chin-up",
                  instructions="A chin-up", type="calisthenics", image="exercise_images/chin_up.png")

    e3.save()
    e3.category.add(p3)
    e3 = Exercise(name="Banded Bicep Curls", description="A banded bicep curl",
                  instructions="A banded bicep curl", type="bands", image="exercise_images/banded_bicep_curls.png")
    e3.save()
    e3.category.add(p2)

    e2 = Exercise(name="Plated Ab Crunch", description="a plated ab crunch",
                  instructions="A plated ab crunch", type="freeweight", image="exercise_images/plate_ab_crunch.png")
    e2.save()
    e2.category.add(p4)
    e3 = Exercise(name="Ab Crunch Machine", description="A machine ab crunch",
                  instructions="A machine ab crunch", type="machine", image="exercise_images/machine_ab_crunch.png")
    e3.save()
    e3.category.add(p4)
    e3 = Exercise(name="Leg Raises", description="A leg raise",
                  instructions="A leg raise", type="calisthenics", image="exercise_images/leg_raises.png")
    e3.save()
    e2.category.add(p4)
    e3 = Exercise(name="Banded Bridge", description="A banded bridge",
                  instructions="A banded bridge", type="bands", image="exercise_images/banded_bridge.png")
    e3.save()
    e3.category.add(p4)

    e2 = Exercise(name="Barbell Squat", description="a squat",
                  instructions="A squat", type="freeweight", image="exercise_images/barbell_squat.png")
    e2.save()
    e2.category.add(p5)
    e3 = Exercise(name="Leg Press", description="A machine leg press",
                  instructions="A machine leg press", type="machine", image="exercise_images/leg_press.png")
    e3.save()
    e3.category.add(p5)
    e3 = Exercise(name="Pistol Squats", description="A pistol squat",
                  instructions="A pistol squat", type="calisthenics", image="exercise_images/pistol_squat.png")
    e3.save()
    e3.category.add(p5)
    e3 = Exercise(name="Banded Leg Extension", description="A banded leg extension",
                  instructions="A banded leg extension", type="bands", image="exercise_images/banded_leg_extension.png")
    e3.save()
    e3.category.add(p5)

    e2 = Exercise(name="Barbell Shrugs", description="a barbell shrug",
                  instructions="A barbell shrug", type="freeweight", image="exercise_images/barbell_shrugs.png")
    e2.save()
    e2.category.add(p6)
    e3 = Exercise(name="Rear Delt Reverse Fly", description="A rear delt reverse fly",
                  instructions="A rear delt reverse fly", type="machine", image="exercise_images/rear_delt_reverse_fly.png")

    e3.save()
    e3.category.add(p5)
    e3 = Exercise(name="Pike Push-Ups", description="A pike push up",
                  instructions="A pike push up", type="calisthenics", image="exercise_images/pike_push_up.png")

    e3.save()
    e3.category.add(p5)
    e3 = Exercise(name="Banded Lateral Raises", description="A banded lateral raise",
                  instructions="A banded lateral raise", type="bands", image="exercise_images/banded_lateral_raise.png")
    e3.save()
    e3.category.add(p5)



##
#

#
