from django.contrib import admin
from .models import UserAccount, Session, Exercise, Set, ExerciseInstance, Category

# Register your models here.
admin.site.register(UserAccount)
admin.site.register(Session)
admin.site.register(Exercise)
admin.site.register(Set)
admin.site.register(ExerciseInstance)
admin.site.register(Category)
