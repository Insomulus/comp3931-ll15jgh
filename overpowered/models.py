from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models import TimeField, ImageField

# Create your models here.


class UserAccount(AbstractUser):
    first_name = models.CharField(max_length=25, blank=False)
    last_name = models.CharField(max_length=25, blank=False)
    height = models.IntegerField(blank=False)
    weight = models.IntegerField(blank=False)
    date_of_birth = models.DateField(blank=False)
    username = models.CharField(primary_key=True,
                                max_length=25, blank=False, unique=True)
    email = models.EmailField(max_length=320, blank=False, unique=True)
    REQUIRED_FIELDS = ['first_name', 'last_name', 'height',
                       'weight', 'date_of_birth', 'email']

    # Changes name of UserAccount to Account
    class Meta:
        verbose_name = 'Account'
        verbose_name_plural = 'Accounts'

    def __str__(self):
        return self.username


class Session(models.Model):
    date = models.DateField(blank=False, unique=True)
    user = models.ForeignKey(UserAccount, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Session'
        verbose_name_plural = 'Sessions'

    def __str__(self):
        return self.user.username + ' | ' + str(self.date)


class Category(models.Model):
    name = models.CharField(max_length=50, blank=False,
                            unique=True, primary_key=True)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Exercise(models.Model):
    name = models.CharField(max_length=50, blank=False,
                            unique=True, primary_key=True)
    description = models.CharField(max_length=200, blank=False)
    instructions = models.CharField(max_length=200, blank=False)
    image = models.ImageField(upload_to='images/', blank=False)

    class Exercise_Type(models.TextChoices):
        FREEWEIGHT = 'freeweight', 'FREEWEIGHT'
        MACHINE = 'machine', 'MACHINE'
        CALISTHENICS = 'calisthenics', 'CALISTHENICS'
        BANDS = 'bands', 'BANDS'

    type = models.CharField(
        max_length=(12), choices=Exercise_Type.choices, default='FREEWEIGHT', blank=False)
    category = models.ManyToManyField(Category)

    class Meta:
        verbose_name = 'Exercise'
        verbose_name_plural = 'Exercises'

    def __str__(self):
        return self.name


class ExerciseInstance(models.Model):
    time = models.TimeField(blank=False, unique=False)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    user = models.ForeignKey(UserAccount, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Exercise Instance'
        verbose_name_plural = 'Exercise Instances'

    def __str__(self):
        return str(self.session) + ' ' + str(self.exercise)


class Set(models.Model):
    time = models.TimeField(blank=False, unique=False)
    instance = models.ForeignKey(ExerciseInstance, on_delete=models.CASCADE)
    weight = models.IntegerField(blank=True, null=True)
    resistance = models.IntegerField(blank=True, null=True)
    bodyweight = models.IntegerField(blank=True, null=True)
    added_weight = models.IntegerField(blank=True, null=True)
    reps = models.IntegerField(blank=False)
    user = models.ForeignKey(UserAccount, on_delete=models.CASCADE)
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Set'
        verbose_name_plural = 'Sets '

    def __str__(self):
        return str(self.time)

#class Diary(models.Model):
#    user = models.OneToOneField(
#        UserAccount, on_delete=models.CASCADE)
#
#    class Meta:
#        verbose_name = 'Diary'
#        verbose_name_plural = 'Diaries'
#
#    def __str__(self):
#        return self.user.username


#class FreeweightSet(models.Model):
#    time = models.TimeField(blank=False, unique=False)
#    session = models.ForeignKey(Session, on_delete=models.CASCADE)
#    exercise = models.ForeignKey(
#            Exercise, limit_choices_to=({'category': 'freeweight'}), on_delete=models.CASCADE)
#    weight = models.IntegerField(blank=False)
#    reps = models.IntegerField(blank=False)
#
#    class Meta:
#        verbose_name = 'Freeweight Set'
#        verbose_name_plural = 'Freeweight Sets'
#
#    def __str__(self):
#        return str(self.exercise)


#class MachineSet(models.Model):
#    session = models.ForeignKey(Session, on_delete=models.CASCADE)
#    exercise = models.ForeignKey(
#            Exercise, limit_choices_to=({'category': 'machine'}), on_delete=models.CASCADE)
#    weight = models.IntegerField()
#    resistance = models.IntegerField()
#    reps = models.IntegerField(blank=False)
#
#    class Meta:
#        verbose_name = 'Machine Set'
#        verbose_name_plural = 'Machine Sets'
#
#    def __str__(self):
#        return str(self.exercise)


#class CalisthenicsSet(models.Model):
#    session = models.ForeignKey(Session, on_delete=models.CASCADE)
#    exercise = models.ForeignKey(
#            Exercise, limit_choices_to=({'category': 'calisthenics'}), on_delete=models.CASCADE)
#    bodyweight = models.IntegerField(blank=False)
#    added_weight = models.IntegerField()
#
#    class Meta:
#        verbose_name = 'Calisthenics Set'
#        verbose_name_plural = 'Calisthenics Sets'
#
#    def __str__(self):
#        return self.exercise


#class BandsSet(models.Model):
#    session = models.ForeignKey(Session, on_delete=models.CASCADE)
#    exercise = models.ForeignKey(
#            Exercise, limit_choices_to=({'category': 'bands'}), on_delete=models.CASCADE)
#    resistance = models.IntegerField(blank=False)
#
#    class Meta:
#        verbose_name = 'Bands Set'
#        verbose_name_plural = 'Bands Sets'
#
#    def __str__(self):
#        return self.exercise
